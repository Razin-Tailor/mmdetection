import os
import re


# MODELS_CONFIG = { 
#         'faster_rcnn_x101_64x4d_fpn_1x':{
#         'config_file': 'configs/faster_rcnn_x101_64x4d_fpn_1x.py'
#      }
# }

# selected_model = 'faster_rcnn_x101_64x4d_fpn_1x'  # 'cascade_rcnn_r50_fpn_1x'

# # Total training epochs.
total_epochs = 8

# # Name of the config file.
# config_file = MODELS_CONFIG[selected_model]['config_file']

# anno_path = './data/VOCdevkit/VOC2007/Annotations'
# voc_file = './mmdet/datasets/voc.py'

classes_names = ['player', 'ball', 'refree']

# fname = voc_file
# with open(fname) as f:
#     s = f.read()
#     s = re.sub('CLASSES = \(.*?\)',
#             'CLASSES = ({})'.format(", ".join(["\'{}\'".format(name) for name in classes_names])), s, flags=re.S)

# with open(fname, 'w') as f:
#     f.write(s)



config_fname = './configs/faster_rcnn_x101_64x4d_fpn_1x.py'
assert os.path.isfile(config_fname), '`{}` not exist'.format(config_fname)
print(config_fname)

cfname = config_fname
with open(cfname) as f:
    s = f.read()
    work_dir = re.findall(r"work_dir = \'(.*?)\'", s)[0]
    # Update `num_classes` including `background` class.
    s = re.sub('num_classes=.*?,',
               'num_classes={},'.format(len(classes_names) + 1), s)
    s = re.sub('ann_file=.*?\],',
               "ann_file=data_root + 'VOC2007/ImageSets/Main/trainval.txt',", s, flags=re.S)
    s = re.sub('total_epochs = \d+',
               'total_epochs = {} #'.format(total_epochs), s)
if "CocoDataset" in s:
    s = re.sub("dataset_type = 'CocoDataset'",
               "dataset_type = 'VOCDataset'", s)
    s = re.sub("data_root = 'data/coco/'",
               "data_root = 'data/VOCdevkit/'", s)
    s = re.sub("annotations/instances_train2017.json",
               "VOC2007/ImageSets/Main/trainval.txt", s)
    s = re.sub("annotations/instances_val2017.json",
               "VOC2007/ImageSets/Main/test.txt", s)
    s = re.sub("annotations/instances_val2017.json",
               "VOC2007/ImageSets/Main/test.txt", s)
    s = re.sub("train2017", "VOC2007", s)
    s = re.sub("val2017", "VOC2007", s)
else:
    s = re.sub('img_prefix=.*?\],',
               "img_prefix=data_root + 'VOC2007/',".format(total_epochs), s)
with open(cfname, 'w') as f:
    f.write(s)
print("Done...")
