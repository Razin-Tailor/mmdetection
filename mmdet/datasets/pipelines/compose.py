import collections

from mmdet.utils import build_from_cfg
from ..registry import PIPELINES


@PIPELINES.register_module
class Compose(object):

    def __init__(self, transforms):
        # print('--- PIPELINES ---')
        print(PIPELINES)

        assert isinstance(transforms, collections.abc.Sequence)
        self.transforms = []
        # print('--- pipeline transforms ---')
        # print(transforms)
        for transform in transforms:
            if isinstance(transform, dict):
                transform = build_from_cfg(transform, PIPELINES)
                # print('--- dict transform --\n\n')
                # print(transform)
                self.transforms.append(transform)
            elif callable(transform):
                # print('\n\n--- callable transform ---')
                # print(transform)
                self.transforms.append(transform)
            else:
                raise TypeError('transform must be callable or a dict')

    def __call__(self, data):
        # print('--- pipeline data ---')
        # print(data)
        for t in self.transforms:
            data = t(data)
            if data is None:
                return None
        return data

    def __repr__(self):
        format_string = self.__class__.__name__ + '('
        for t in self.transforms:
            format_string += '\n'
            format_string += '    {0}'.format(t)
        format_string += '\n)'
        return format_string
