
from mmdet.models import build_detector
from mmdet.apis import inference_detector, show_result, init_detector
import os
imgs = os.listdir('/home/usergpu/zinmmdet/mmdetection/stanneb/stanneb/jpg/')
imgs = ['/home/usergpu/zinmmdet/mmdetection/stanneb/stanneb/jpg/'+x for x in imgs]
save_dir = './boxes_faster_rcnn_orig_stanneb/'
config_file = './configs/faster_rcnn_x101_64x4d_fpn_1x.py'
checkpoint_file = '/home/usergpu/zinmmdet/mmdetection/work_dirs/faster_rcnn_x101_64x4d_fpn_1x/latest.pth'
# checkpoint_file = './checkpoints/faster_rcnn_x101_64x4d_fpn_1x_20181218-c9c69c8f.pth'

model = init_detector(config_file, checkpoint_file)

score_thr = 0.8

for img in imgs:
    result = inference_detector(model, img)
    show_result(img, result, model.CLASSES, out_file='./test_results_faster_rcnn_stanneb/{}'.format(img.split('/')[-1]), score_thr=score_thr, show=False, save_dir = save_dir)






#for i, result in enumerate(inference_detector(model, imgs)):
#    show_result(imgs[i], result, model.CLASSES, out_file='./test_results/{}'.format(imgs[i].split('/')[-1]), score_thr=score_thr, show=False)

# test a video and show the results
