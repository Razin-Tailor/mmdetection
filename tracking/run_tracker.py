# -*- coding: utf-8 -*-

""" Deep SORT is based on MOT Challenge Data


---


The file format should be the same as the ground truth file, which is a CSV text-file containing one object instance per line. Each line must contain 10 values:

`<frame>, <id>, <bb_left>, <bb_top>, <bb_width>, <bb_height>, <conf>, <x>, <y>, <z>`
The conf value contains the detection confidence in the det.txt files. For the ground truth, it acts as a flag whether the entry is to be considered. A value of 0 means that this particular instance is ignored in the evaluation, while any other value can be used to mark it as active. For submitted results, all lines in the .txt file are considered. The world coordinates x,y,z are ignored for the 2D challenge and can be filled with -1. Similarly, the bounding boxes are ignored for the 3D challenge. However, each line is still required to contain 10 values.

All frame numbers, target IDs and bounding boxes are 1-based. Here is an example:
  
Tracking with bounding boxes

---


(2D MOT 2015, MOT16, PETS2017, MOT17)

  1, 3, 794.27, 247.59, 71.245, 174.88, -1, -1, -1, -1
  
  1, 6, 1648.1, 119.61, 66.504, 163.24, -1, -1, -1, -1
  
  1, 8, 875.49, 399.98, 95.303, 233.93, -1, -1, -1, -1

These steps are done to modify some tracking

---

`copied from issue #111 on original repo `

I had modified these parameter or operating logic :

1.   Threshold-replace 0.2 by 0.25
2.   Average the top five features of the track as final distance
OR average the top 3rd to 5th features, i.e. remove the top 2 features
3.   Every frame will carry out these steps that include tracker.predict() and tracker.update() regardless of whether or not having a detection.
4.   For matching cascade, trackers whose age<=5 are gathered together to associate at the same
time

---

For point #2

In  nn_matching.py file,  modifying  corresponding code as  below:

      distances.sort(axis=0)    # sorted firstly
      return distances[2:5].mean(axis=0)  #average  2-5  features

"""

""" Imports """

# Basic Imports

import os
import sys
import os.path as osp
import pickle

if os.getcwd() not in sys.path:
    sys.path.append(os.getcwd())

import spordia.Box as box

# Computer Vision Libraries Import

import cv2

# Clipping Library
from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip

# Data Manipulation and plotting Imports
import numpy as np

import matplotlib.pyplot as plt

# Deep-SORT imports

import tracking.tools.generate_detections as generate_detections
from tracking.deep_sort.detection import Detection
from tracking.application_util import preprocessing
from tracking.deep_sort import nn_matching
from tracking.deep_sort.tracker import Tracker

import matplotlib.patches as patches
import argparse
import pandas as pd
from collections import defaultdict

import warnings

warnings.filterwarnings("ignore", category=DeprecationWarning)

base_path = "/Users/sportsseam/Desktop/projects/cv/"
DEEP_SORT_MODEL_FILE_PATH = base_path + 'vireal-cv/tracking/resources/models/mars-small128.pb'
CLIP_PATH = base_path + 'vireal-cv/tracking/resources/clips/stanfordalabama_rally_01.mp4'
FILTERED_BOX_FILE_PATH = '/tmp/'
SP_MOT_TXT_FILE_PATH = base_path + 'vireal-cv/tracking/resources/tempfiltered_player_person_new_corner.txt'
NPY_FILE_PATH = base_path + 'vireal-cv/tracking/resources/detcorrected_filtered_corner_ref.npy'

OUTPUT_PATH = base_path + 'vireal-cv/tracking/output/'

NO_OF_TRACKS = 15
MIN_TRACK_LENGTH = 30

state_map = {1: 'Tentative',
             2: 'Confirmed',
             3: 'Deleted'
             }


_court_homography_map = dict()
_court_homography_map["stanford_orig"] = \
    np.array([[2.54189554e+00, 2.05548343e+00, -1.71490081e+03],
              [4.59969941e-02, 1.45810471e+01, -4.16148605e+03],
              [3.71823046e-05, 2.65221085e-03, 1.00000000e+00]])

_court_homography_map["stanford_480"] = \
    np.array([[3.58116231e+00, 2.49261517e+00, -1.39335611e+03],
              [-1.85560132e-01, 2.08755149e+01, -4.27576935e+03],
              [ 1.96505291e-06, 3.80235925e-03, 1.00000000e+00]])

_court_homography_map["stanford_blue"] = \
    np.array([[1.38019562e+00, 4.16755338e-01, -4.87353692e+02],
              [3.60155502e-02, 5.44555119e+00, -8.60526743e+02],
              [1.07991316e-05, 9.72631616e-04, 1.00000000e+00]])

_court_homography_map["stanford_brown"] = \
    np.array([[1.40750079e+00, 4.28041386e-01, -5.01996866e+02],
              [ 3.69169984e-02, 5.53754976e+00, -8.97378396e+02],
              [ 1.67593482e-05, 9.94257909e-04, 1.00000000e+00]])


def get_court_homogenous_position(left, right, bottom, homology=None, ):
    # homolgoy is calculated for bottom only. Easy to be extended for any point such as top, corner, center etc.
    # print("Using %s homography" % homology)
    x = (left + right) / 2.0
    y = bottom
    if homology is None:
        print("Homology is required.. Cannot proceed!")
        print("Quitting")
        sys.exit(-1)
        # predefine homography for volleball court based on stanford camera
    else:
        homology_matrix = _court_homography_map[homology]
    p = np.array((x, y, 1)).reshape((3, 1))
    temp_p = homology_matrix.dot(p)
    sum_z = np.sum(temp_p, 1)
    bottom_homology_x = int(round(sum_z[0] / sum_z[2]))
    bottom_homology_y = int(round(sum_z[1] / sum_z[2]))
    return bottom_homology_x, bottom_homology_y

""" Process the Dataframe to Get the rally time (One Rally for now Manually) \
Assuming StanfordScout.csv is in path `/content/` and clip"""


# Using Stanford Brown viz Stanford vs Alabama
def get_start_end_time():
    '''
    Update this function to get the start and end time

    '''
    return (674, 693)


""" MOT Challenge (DeepSORT inherently) requires data file in different format than the one we use to write boxes

---

The following function makes the corresponding txt file (can be merged in the `inference` code of `mmdet` in future)
"""


def make_mot_from_box(base_path, all_frame_files, file_out):
    """
    (y1,y2,x1,x2) => (x1,y1,w,h)
    """

    def get_xywh(line):
        y1, y2, x1, x2, label, idx, conf = line.split(',')

        x = x1
        y = y1
        w = int(float(x2) - float(x1))
        h = int(float(y2) - float(y1))
        return x, y, w, h, conf

    with open(file_out, 'w') as file_out:
        for frame_file in all_frame_files:
            frame_idx = int(frame_file.split('.')[0])
            print("Frame-idx", frame_idx)
            arr = []
            with open(base_path + frame_file, 'r') as file_in:
                lines = file_in.readlines()
                for line in lines:
                    print(line)
                    x, y, w, h, conf = get_xywh(line)
                    print(x, y, w, h, conf)
                    coord = [x, y, w, h, conf]
                    padding = np.array([-1, -1, -1])
                    coord = np.insert(coord, 0, frame_idx)
                    coord = np.insert(coord, 1, -1)
                    coord = np.append(coord, padding)
                    resp = ','.join([str(float(x.strip())) for x in coord])
                    print(resp)
                    resp += '\n'
                    file_out.write(resp)
            print(arr)


def create_detections(detection_mat, frame_idx, min_height=0):
    """Create detections for given frame index from the raw detection matrix.

    Parameters
    ----------
    detection_mat : ndarray
        Matrix of detections. The first 10 columns of the detection matrix are
        in the standard MOTChallenge detection format. In the remaining columns
        store the feature vector associated with each detection.
    frame_idx : int
        The frame index.
    min_height : Optional[int]
        A minimum detection bounding box height. Detections that are smaller
        than this value are disregarded.

    Returns
    -------
    List[tracker.Detection]
        Returns detection responses at given frame index.

    """
    frame_indices = detection_mat[:, 0].astype(np.int)
    mask = frame_indices == frame_idx

    detection_list = []
    # returning from here the bbox list corresponding to the frame 

    frame_bboxes = {frame_idx: []}

    for row in detection_mat[mask]:
        bbox, confidence, feature = row[2:6], row[6], row[10:]
        frame_bboxes[frame_idx].append(bbox)
        if bbox[3] < min_height:
            continue
        detection_list.append([Detection(bbox, confidence, feature), (frame_idx, bbox)])

    return detection_list


def make_npy(encoder, video, name_out, detection):
    detections_in = np.array(detection)
    detections_out = []
    print(type(detections_in), detections_in[:, 0])
    frame_indices = detections_in[:, 0].astype(np.int)
    min_frame_idx = frame_indices.astype(np.int).min()
    max_frame_idx = frame_indices.astype(np.int).max()

    camera = cv2.VideoCapture(video)

    for frame_idx in range(min_frame_idx, max_frame_idx + 1):
        print("Frame %05d/%05d" % (frame_idx, max_frame_idx))
        mask = frame_indices == frame_idx
        rows = detections_in[mask]

        if frame_idx not in frame_indices:
            print("WARNING could not find image for frame %d" % frame_idx)
            continue
        camera.set(cv2.CAP_PROP_POS_FRAMES, frame_idx - 1);
        (grabbed, bgr_image) = camera.read()
        features = encoder(bgr_image, rows[:, 2:6].copy())
        detections_out += [np.r_[(row, feature)] for row, feature
                           in zip(rows, features)]

    np.save(NPY_FILE_PATH, np.asarray(detections_out), allow_pickle=False)


def generate_detections_video(video, name_out, video_name, csv=False, homology=None):
    """Generate detections with features. Modification with video
    """

    fourcc = cv2.VideoWriter_fourcc(*"XVID")
    out = cv2.VideoWriter(video_name, fourcc, 30.0, (1280, 720))

    def print_track_info(track_info):

        print("Track info %s --" % len(track_info.keys()), [(t, len(track_info[t])) for t in track_info])

    # For Tracking
    pickle_file = "/tmp/track_info.pickle"
    if os.path.isfile(pickle_file):
        with open(pickle_file, "rb") as f:
            track_info = pickle.load(f)
    else:
        track_info = create_track_info(name_out, video, video_name)
        with open(pickle_file, "wb") as f:
            pickle.dump(track_info, f)

    track_counts = get_track_counts(track_info)
    remove_small_tracks(track_info, track_counts)
    continue_merge = True
    while continue_merge:
        print_track_info(track_info)
        track_counts = get_track_counts(track_info)
        mergers = get_possible_mergers(track_info)

        # continue_merge = merge_tracks(mergers, track_info, track_counts)
        continue_merge = merge_tracks_longest_longest_first(mergers, track_info, track_counts)
        # continue_merge = merge_tracks_create_longest(mergers, track_info, track_counts)

    print("Finale tracks")
    # print_track_info(track_info)
    create_output(video, track_info, out, csv, homology)


def create_track_info(name_out, video, video_name):
    max_cosine_distance = 0.2
    nn_budget = None
    metric = nn_matching.NearestNeighborDistanceMetric(
        "cosine", max_cosine_distance, nn_budget)
    tracker = Tracker(metric)
    detections_file = np.load(name_out)
    camera = cv2.VideoCapture(video)
    frame_idx = 0;
    min_frame_idx = int(detections_file[:, 0].min())
    max_frame_idx = int(detections_file[:, 0].max())
    min_confidence = 0.80
    min_detection_height = 0
    nms_max_overlap = 0.7
    i = 0
    none_frames = []
    track_info = dict()
    while True:
        (grabbed, frame) = camera.read()
        frame_idx += 1

        if frame is None:
            none_frames.append(frame_idx)
        if 186 < frame_idx < 196:
            print("\n\n\nProcessing frame %05d\n\n\n" % frame_idx)

        # Load image and generate detections.
        detections = create_detections(detections_file, frame_idx - 1, min_detection_height)

        # detections = [d for d in detections if d.confidence >= min_confidence]

        # # Run non-maxima suppression.
        # boxes = np.array([d.tlwh for d in detections])
        # scores = np.array([d.confidence for d in detections])
        # indices = preprocessing.non_max_suppression(boxes, nms_max_overlap, scores)
        # detections = [detections[i] for i in indices]

        # Update tracker.
        tracker.predict()
        # tracker.update(detections)
        tracker.update(detections)

        # Store results.
        results = []
        untracked_results = []

        # Store results.
        for track in tracker.tracks + tracker.deleted_tracks:
            # if not track.is_confirmed() or track.time_since_update > 1:
            #     continue
            track_info[track.track_id] = track.detections

            id_num = str(track.track_id)  # Get the ID for the particular track.

            features = track.features

            bbox = track.to_tlbr()  # .to_tlwh()
            # cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])),(255,255,255), 2)
            # cv2.putText(frame, str(id_num),(int(bbox[0]), int(bbox[1])),0, 5e-3 * 200, (0,255,0),2)

            results.append([
                frame_idx, track.track_id, state_map[track.state], track.detections, bbox[0], bbox[1], bbox[2],
                bbox[3]])

        if grabbed == False:
            break

            # print('%d,%d,%s,%.2f,%.2f,%.2f,%.2f,1,-1,-1,-1' % (
            #     row[0], row[1], row[2], row[3], row[4], row[5], row[6]),file=f)
    print("--- Post Processing Begins ---")
    return track_info


def get_track_counts(track_info):
    track_counts = dict()

    for key, value in track_info.items():
        count = len([v[0] for v in value])
        track_counts[key] = count

    return track_counts


def get_possible_mergers(track_info):
    frames_for_track = dict()
    for key, value in track_info.items():
        frames_for_track[key] = set([v[0] for v in value])

    to_merge_dict = defaultdict(list)

    for i in track_info.keys():
        set_for_frame_i = frames_for_track[i]
        for j in track_info.keys():
            if i < j:
                set_for_frame_j = frames_for_track[j]
                if len(set_for_frame_i.intersection(set_for_frame_j)) == 0:
                    to_merge_dict[i].append(j)
                    # print("{} has NO common frames with {}".format(i,j))
    print("Track that can be merged %s", [(k, to_merge_dict[k]) for k in to_merge_dict])
    # frame_2 = list(frames_for_track[2])
    # frame_4 = list(frames_for_track[4])
    # frame_2.sort()
    # frame_4.sort()
    #
    # print("Track 2 frame ", frame_2)
    # print("Track 4 frame ", frame_4)

    return to_merge_dict


def merge(parent_track, child_track, track_info):
    """
    Append the values of `child_track` to values of `parent_track`
    Delete the `child_track` Node
    """
    print("Merging track %s with %s " % (parent_track, child_track))
    child_track_values = track_info[child_track]

    track_info[parent_track].extend(child_track_values)
    del track_info[child_track]


def remove_small_tracks(track_info, track_counts):
    to_be_delete_tracks = [t for t in track_counts if track_counts[t] < MIN_TRACK_LENGTH]
    for t in to_be_delete_tracks:
        del track_info[t]


"""
- The mergers have the information of the tracks that can be potentially merged together into 1
- But the actual information about the track rests with the `track_info` data_structure 
- So when the merge routine is in action the end results should be merged in the `track_info` data_structure

"""


def merge_tracks_longest_longest_first(mergers, track_info, track_counts):
    """
    Get longest track in the possible tracks and merge it with the parent
    """

    continue_merge = False
    track_lengths = [(t, track_counts[t]) for t in track_counts]
    track_lengths.sort(key=lambda x: x[1], reverse=True)

    # select the longest track first.

    for track_length in track_lengths:
        selected_track = track_length[0]
        possible_merge_tracks = mergers[selected_track]
        merge_track_lengths = [(t, track_counts[t]) for t in possible_merge_tracks]
        merge_track_lengths.sort(key=lambda x: x[1], reverse=True)
        if len(merge_track_lengths) > 0:
            best_match_track = merge_track_lengths[0][0]
            merge(selected_track, best_match_track, track_info)
            continue_merge = True
            break
    return continue_merge


def merge_tracks_create_longest(mergers, track_info, track_counts):
    """
    Get longest track in the possible tracks and merge it with the parent
    """

    continue_merge = False
    track_pairs = []  # (track, merge_track), combined length

    for t in track_counts:
        possible_merge_tracks = mergers[t]
        for mt in possible_merge_tracks:
            if mt > t:
                track_pairs.append([t, mt, track_counts[t] + track_counts[mt]])
    track_pairs.sort(key=lambda x: x[2], reverse=True)

    for track_pair in track_pairs:
        merge(track_pair[0], track_pair[1], track_info)
        continue_merge = True
        break
    return continue_merge


def merge_tracks(mergers, track_info, track_counts):
    """
    Get longest track in the possible tracks and merge it with the parent
    """
    continue_merge = False

    for track, possible_mergers_with_track in mergers.items():
        count_list = []
        for possible_track in possible_mergers_with_track:
            count_list.append((possible_track, track_counts[possible_track]))
        # print(count_list)
        count_list.sort(key=lambda x: x[1], reverse=True)
        if len(mergers.items()) < 4:
            print("Track considered ", track, "--", count_list)
        if len(count_list) == 0:
            return track_info, continue_merge
        else:
            best_match = count_list[0]
            if len(mergers.items()) < 4:
                print("Track {}".format(track))
                print("\n\n\n ==>Best Track to Merge With Track {} is {} <==".format(track, best_match[0]))
                print("----track_info before", track_info.keys())
            merge(track, best_match[0], track_info)
            continue_merge = True
            if len(mergers.items()) < 4:
                print("----track_info after", track_info.keys())
            break

    return continue_merge


def create_output(video, track_info, out, csv=False, homology=None):
    frame_idx = 0
    camera = cv2.VideoCapture(video)
    none_frames = []
    csv_data = list()
    while True:
        (grabbed, frame) = camera.read()
        # print(grabbed)
        frame_idx += 1

        if frame is None:
            none_frames.append(frame_idx)

        # print("Processing frame %05d" % frame_idx)
        bbox_list = dict()

        for track, info in track_info.items():
            for item in info:
                frame_no, bbox_array = item
                if frame_no == frame_idx:
                    bbox_list[track] = bbox_array

        # print(len(bbox_list))
        for id_num, bbox in bbox_list.items():
            top_left_x = int(bbox[0])
            top_left_y = int(bbox[1])
            width = int(bbox[2])
            height = int(bbox[3])

            bottom_right_x = top_left_x + width
            bottom_right_y = top_left_y + height
            left_top = (top_left_x, top_left_y)
            right_bottom = (bottom_right_x, bottom_right_y)

            cv2.rectangle(frame, left_top, right_bottom, (255, 255, 255), 2)
            cv2.putText(frame, str(id_num), (int(bbox[0]), int(bbox[1])), 0, 5e-3 * 200, (0, 255, 0), 2)

            if csv:
                bottom_homology_x, bottom_homology_y = get_court_homogenous_position(top_left_x, bottom_right_x,
                                                                                     bottom_right_y, homology)

                csv_data.append([frame_idx, id_num, bottom_homology_x, bottom_homology_y])

        if frame is not None:
            # cv2.imwrite('./'+str(frame_idx)+'.jpg', frame)
            cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
            out.write(frame)

        if grabbed == False:
            break
    if csv:
        track_info_dict = defaultdict(list)
        columns = ['frame_no']
        for i in range(1,13):
            column_x = 'player_{}_bottom_homology_x'.format(str(i))
            column_y = 'player_{}_bottom_homology_y'.format(str(i))
            columns.append(column_x)
            columns.append(column_y)
        for data in csv_data:
            frame_idx, id_num, bottom_homology_x, bottom_homology_y = data
            if id_num in track_info_dict.keys():
                track_info_dict[id_num].append([frame_idx, bottom_homology_x, bottom_homology_y])
            else:
                track_info_dict[(id_num, frame_idx)] = [[bottom_homology_x, bottom_homology_y]]
        print(columns)
        # print(track_info_dict.keys())
        csv_data = np.array(csv_data)
        min_frame_no = min(csv_data[:,0])
        max_frame_no = max(csv_data[:, 0])
        print("Max {} Min {}".format(max_frame_no, min_frame_no))
        # print("Track Info Dict for Track 1:: ", len(track_info_dict[1]))

        for frame_no in range(min_frame_no, max_frame_no+1):
            for track_id in range(1,13):
                key_in_consideration = (track_id, frame_no)
                if key_in_consideration not in track_info_dict.keys():
                    track_info_dict[key_in_consideration] = [[None, None]]
        print("Total hova joiye {}".format(578*12))
        print(len(track_info_dict.keys()))

        final_list = list()
        for frame_no in range(min_frame_no, max_frame_no+1):
            tracks_list = list()
            tracks_list.append(frame_no)
            for track_id in range(1,13):

                data =  track_info_dict[(track_id, frame_no)]
                print(data[0])
                x, y = data[0]
                tracks_list.append(x)
                tracks_list.append(y)
            final_list.append(tracks_list)

        print("--- final ---")
        print(len(final_list), len(final_list[0]))

        df = pd.DataFrame(final_list, columns=columns)
        df.to_csv('stan_alab_v1.csv', columns=columns, index=False)


def create_clip(infile, outfile, start_time, end_time):
    infile = '/content/drive/My Drive/2018.12.01-04.33-83725 stanford vs albama state.mp4'
    out_file = '/content/stanfordalabama_rally_01.mp4'

    start, end = get_start_end_time()

    ffmpeg_extract_subclip(infile, start, end, targetname=out_file, )
    print('-- {} : created --'.format(out_file))


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--create_clip", type=bool, default=False, help="create clip from match video")
    parser.add_argument("--create_mot_txt", type=bool, default=False,
                        help="create txt file in MOT format from the predicted box files")
    parser.add_argument("--create_npy", type=bool, default=False,
                        help="create the numpy file with the DeepSORT detections")
    parser.add_argument("--track", type=bool, default=False, help="Run the tracker and save the output")
    parser.add_argument("--homology", type=str, default=None,
                        help="Homology to transform picel to real-world co ord \
                        (`stanford_brown`, `stanford_blue`, `stanford_orig`, `stanford_480`)")

    parser.add_argument("--csv", type=bool, default=False, help="Output Data in csv Form")

    opt = parser.parse_args()

    if opt.create_clip:
        match_path = './'
        clip_path = './'
        start_time = 674
        end_time = 693

        create_clip(match_path, clip_path, start_time, end_time)

    if opt.create_mot_txt:
        all_frame_files = [x for x in os.listdir(FILTERED_BOX_FILE_PATH) if 'box' in x]
        make_mot_from_box(base_path, all_frame_files, file_out=SP_MOT_TXT_FILE_PATH)

    if opt.create_npy:

        model_filename = DEEP_SORT_MODEL_FILE_PATH  # '/content/drive/My Drive/deep_sort/networks/mars-small128.pb'
        input_name = "images"
        output_name = "features"
        batch_size = 32

        print(model_filename)
        encoder = generate_detections.create_box_encoder(model_filename, batch_size=32)

        with open(SP_MOT_TXT_FILE_PATH, 'r') as fin:  # '/content/tempfiltered_player_person_new_corner.txt'
            lines = fin.readlines()
            final = []
            for line in lines:
                splitted = line.split(',')
                final.append([float(x) for x in splitted])

        make_npy(encoder, CLIP_PATH, NPY_FILE_PATH, final)

    if opt.track:
        if opt.csv and opt.homology is None:
            print("Homology Inormation is needed to generate csv")
            sys.exit(-1)

        model_filename = DEEP_SORT_MODEL_FILE_PATH  # '/content/drive/My Drive/deep_sort/networks/mars-small128.pb'
        input_name = "images"
        output_name = "features"
        batch_size = 32
        csv_flag = opt.csv
        homology = opt.homology

        print(model_filename)
        encoder = generate_detections.create_box_encoder(model_filename, batch_size=32)

        with open(SP_MOT_TXT_FILE_PATH, 'r') as fin:  # '/content/tempfiltered_player_person_new_corner.txt'
            lines = fin.readlines()
            final = []
            for line in lines:
                splitted = line.split(',')
                final.append([float(x) for x in splitted])

        generate_detections_video(CLIP_PATH, NPY_FILE_PATH,
                                  OUTPUT_PATH + 'experiment_60_age.avi', csv=csv_flag, homology=homology)  # homology_ref ceontroid_and_homology_ref
