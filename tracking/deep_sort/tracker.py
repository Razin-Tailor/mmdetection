# vim: expandtab:ts=4:sw=4
from __future__ import absolute_import
import numpy as np
from . import kalman_filter
from . import linear_assignment
from . import iou_matching
from .track import Track
from shapely.geometry import Polygon
import copy
import sys
import pdb

import os
import sys
import os.path as osp
import pickle

if os.getcwd() not in sys.path:
    sys.path.append(os.getcwd())

import spordia.Box as box


class Tracker:
    """
    This is the multi-target tracker.

    Parameters
    ----------
    metric : nn_matching.NearestNeighborDistanceMetric
        A distance metric for measurement-to-track association.
    max_age : int
        Maximum number of missed misses before a track is deleted.
    n_init : int
        Number of consecutive detections before the track is confirmed. The
        track state is set to `Deleted` if a miss occurs within the first
        `n_init` frames.

    Attributes
    ----------
    metric : nn_matching.NearestNeighborDistanceMetric
        The distance metric used for measurement to track association.
    max_age : int
        Maximum number of missed misses before a track is deleted.
    n_init : int
        Number of frames that a track remains in initialization phase.
    kf : kalman_filter.KalmanFilter
        A Kalman filter to filter target trajectories in image space.
    tracks : List[Track]
        The list of active tracks at the current time step.

    """

    def __init__(self, metric, max_iou_distance=0.7, max_age=60, n_init=20):
        self.metric = metric
        self.max_iou_distance = max_iou_distance
        self.max_age = max_age
        self.n_init = n_init

        self.kf = kalman_filter.KalmanFilter()
        self.tracks = []
        self.deleted_tracks = []
        self._next_id = 1
        self._court_homography_map = dict()
        self._court_homography_map["stanford_orig"] = \
            np.array([[2.54189554e+00, 2.05548343e+00, -1.71490081e+03],
                      [4.59969941e-02, 1.45810471e+01, -4.16148605e+03],
                      [3.71823046e-05, 2.65221085e-03, 1.00000000e+00]])

        self._court_homography_map["stanford_480"] = \
            np.array([[3.58116231e+00, 2.49261517e+00, -1.39335611e+03],
                      [-1.85560132e-01, 2.08755149e+01, -4.27576935e+03],
                      [1.96505291e-06, 3.80235925e-03, 1.00000000e+00]])

        self._court_homography_map["stanford_blue"] = \
            np.array([[1.38019562e+00, 4.16755338e-01, -4.87353692e+02],
                      [3.60155502e-02, 5.44555119e+00, -8.60526743e+02],
                      [1.07991316e-05, 9.72631616e-04, 1.00000000e+00]])

        self._court_homography_map["stanford_brown"] = \
            np.array([[1.40750079e+00, 4.28041386e-01, -5.01996866e+02],
                      [3.69169984e-02, 5.53754976e+00, -8.97378396e+02],
                      [1.67593482e-05, 9.94257909e-04, 1.00000000e+00]])

    def predict(self):
        """Propagate track state distributions one time step forward.

        This function should be called once every time step, before `update`.
        """
        for track in self.tracks:
            track.predict(self.kf)

    def update(self, detections):
        """Perform measurement update and track management.

        Parameters
        ----------
        detections : List[deep_sort.detection.Detection]
            A list of detections at the current time step.

        """
        # Run matching cascade.

        if len(detections) > 12:
            detections = detections[:12]
        matches, unmatched_tracks, unmatched_detections = \
            self._match(detections)

        # Update track set.
        for track_idx, detection_idx in matches:
            self.tracks[track_idx].update(
                self.kf, detections[detection_idx])
        # CHANGE BY sunilkul.
        # for track_idx in unmatched_tracks:
        #     # Do not mark any track as missed or deleted.
        #     self.tracks[track_idx].mark_missed()
        #     print("-- Missed Track -- ", track_idx, self.tracks[track_idx])
        for detection_idx in unmatched_detections:
            print("SHOULD NOT REACH HERE AS WE ARE INITIATING NEW TRACKS.")
            # print("-- Unmatched Detection -- ", detection_idx, detections[detection_idx])
            self._initiate_track(detections[detection_idx])
            sys.exit(-1)

        self.deleted_tracks = [t for t in self.tracks if t.is_deleted()]
        self.tracks = [t for t in self.tracks if not t.is_deleted()]

        # Update distance metric.
        active_targets = [t.track_id for t in self.tracks if t.is_confirmed()]
        features, targets = [], []
        for track in self.tracks:
            if not track.is_confirmed():
                continue
            features += track.features
            targets += [track.track_id for _ in track.features]
            track.features = []
        self.metric.partial_fit(
            np.asarray(features), np.asarray(targets), active_targets)

    def _match(self, detections):

        detections_copy = copy.deepcopy(detections)

        bbox_info = [d[1] for d in detections]
        detections = [d[0] for d in detections]

        def gated_metric(tracks, dets, track_indices, detection_indices):
            features = np.array([dets[i].feature for i in detection_indices])
            targets = np.array([tracks[i].track_id for i in track_indices])
            cost_matrix = self.metric.distance(features, targets)
            # print('-- gated metric --')
            # print(cost_matrix.shape)
            # print('# Tracks {}'.format(len(tracks)))
            # print('# targets {}'.format(len(targets)))
            cost_matrix = linear_assignment.gate_cost_matrix(
                self.kf, cost_matrix, tracks, dets, track_indices,
                detection_indices)
            # print('-- post gated cost metric --')
            # print(cost_matrix.shape)
            return cost_matrix

        # Split track set into confirmed and unconfirmed tracks.
        confirmed_tracks = [
            i for i, t in enumerate(self.tracks) if t.is_confirmed()]
        unconfirmed_tracks = [
            i for i, t in enumerate(self.tracks) if not t.is_confirmed()]

        # Associate confirmed tracks using appearance features.
        print("---- Appearance Feature Matching Begins ----")
        # distance_metric, max_distance, cascade_depth, tracks, detections,
        # track_indices = None, detection_indices = None, bbox_info = None
        matches_a, unmatched_tracks_a, unmatched_detections = \
            linear_assignment.matching_cascade(
                gated_metric, self.metric.matching_threshold, self.max_age,
                self.tracks, detections, confirmed_tracks, bbox_info=bbox_info)

        # Associate remaining tracks together with unconfirmed tracks using IOU.

        print("---- IOU Matching Begins ----")
        iou_track_candidates = unconfirmed_tracks + [
            k for k in unmatched_tracks_a if
            self.tracks[k].time_since_update == 1]
        unmatched_tracks_a = [
            k for k in unmatched_tracks_a if
            self.tracks[k].time_since_update != 1]

        # I will need dets bbox info and frame number here

        matches_b, unmatched_tracks_b, unmatched_detections = \
            linear_assignment.min_cost_matching(
                iou_matching.iou_cost, self.max_iou_distance, self.tracks,
                detections, iou_track_candidates, unmatched_detections, bbox_info=bbox_info)

        matches = matches_a + matches_b
        unmatched_tracks = list(set(unmatched_tracks_a + unmatched_tracks_b))
        # Added by sunilkul.

        # self, detections_copy, matches, unmatched_tracks,
        print("--- Unmatched Detection ---")
        print(unmatched_detections)
        matches_c, unmatched_tracks_c, unmatched_detections = \
            self._handle_unmatched_detection_and_tracks(detections_copy, matches, unmatched_tracks,
                                                        unmatched_detections)

        return matches_c, unmatched_tracks_c, unmatched_detections

    def _print_unmatched_track_ids(self, unmatched_tracks):
        print([self.tracks[t].track_id for t in unmatched_tracks])
        return [self.tracks[t].track_id for t in unmatched_tracks]

    def _get_track_from_track_id(self, track_id):
        # print([t.track_id for t in self.tracks])
        # print(track_id)
        return [t for t in self.tracks if t.track_id == track_id][0]

    def _get_prev_frame_bbox_info(self, frame_no, unmatched_tracks):
        print(unmatched_tracks)
        prev_frame = frame_no - 1
        prev_frame_bbox_info = dict()
        for track_idx in unmatched_tracks:
            track = self.tracks[track_idx]
            print(track.track_id)
            detections = track.detections[-1]
            prev_frame_bbox_info[track.track_id] = detections

        return prev_frame_bbox_info

    def _perform_centroid_based_allocation(self, curr_bbox_info, detection, unmatched_tracks, matches, frame_no):
        def _get_polygon_co_ord(bbox):
            print("-- get polygon co ord --", bbox)
            width = int(bbox[2])
            height = int(bbox[3])

            left_x = int(bbox[0])
            top_y = int(bbox[1])
            right_x = left_x + width
            bottom_y = top_y + height

            return Polygon([(left_x, top_y), (right_x, top_y), (right_x, bottom_y), (left_x, bottom_y)])

        def _get_centroid_distance(current_bbox, prev_bbox):
            current_bbox_polygon = _get_polygon_co_ord(current_bbox)
            current_bbox_centroid = current_bbox_polygon.centroid

            prev_bbox_polygon = _get_polygon_co_ord(prev_bbox)
            prev_bbox_centroid = prev_bbox_polygon.centroid

            return current_bbox_centroid.distance(prev_bbox_centroid)

        print("--- Unmatched Tracks ---")
        self.print_unmatched_track_ids(unmatched_tracks)
        # ---- Centroid Logic Here ----

        prev_frame_bbox_info = self._get_prev_frame_bbox_info(frame_no, unmatched_tracks)
        print("Prev Frame {}".format(frame_no - 1))
        print(prev_frame_bbox_info)

        centroid_distance_dict = {track: _get_centroid_distance(curr_bbox_info, prev_frame_bbox)
                                  for track, prev_frame_bbox in prev_frame_bbox_info.items()}

        print("Centroid Distance", centroid_distance_dict)
        sorted_centroid_track_list = [(k, centroid_distance_dict[k])
                                      for k in sorted(centroid_distance_dict,
                                                      key=centroid_distance_dict.get, reverse=False)]
        print(sorted_centroid_track_list)
        first_track_id_in_sorted_list = sorted_centroid_track_list[0][0]
        to_be_assigned_track = [t for t in self.tracks if t.track_id == first_track_id_in_sorted_list][0]

        print("-- To be Assigned Track --", to_be_assigned_track.track_id)
        index = self.tracks.index(to_be_assigned_track)
        print("Track {} Index {}".format(to_be_assigned_track.track_id, index))

        unmatched_tracks.remove(index)
        self.tracks[index].update(self.kf, detection)
        # Do we need to update matches?
        return unmatched_tracks, matches

    def _perform_homology_based_allocation(self, curr_bbox_info, detection, unmatched_tracks, matches, frame_no):
        def _get_homology_co_ord(bbox, homology_matrix):
            width = int(bbox[2])
            height = int(bbox[3])

            left_x = int(bbox[0])
            top_y = int(bbox[1])
            right_x = left_x + width
            bottom_y = top_y + height

            x = (left_x + right_x) / 2.0
            y = bottom_y

            p = np.array((x, y, 1)).reshape((3, 1))
            temp_p = homology_matrix.dot(p)
            sum_z = np.sum(temp_p, 1)

            bottom_homology_x = int(round(sum_z[0] / sum_z[2]))
            bottom_homology_y = int(round(sum_z[1] / sum_z[2]))

            return bottom_homology_x, bottom_homology_y

        def _get_euclidian_distance(point_1, point_2):
            x1, y1 = point_1
            x2, y2 = point_2
            return np.sqrt(((x2 - x1) ** 2) + ((y2 - y1) ** 2))

        prev_frame_bbox_info = self._get_prev_frame_bbox_info(frame_no, unmatched_tracks)
        print("Prev Frame {}".format(frame_no - 1))
        print(prev_frame_bbox_info)

        # (xtl, ytl) => (709, 217)
        # (xbr, xbr) => (794, 462)
        print("================================================================================")
        print("-- DEBUG HOMOLOGY BEGIN -- ")
        print("---- Frame in Consideration: {} ----".format(frame_no))
        print("---- Unmatched BBox: {} ----".format(curr_bbox_info))
        x = self._print_unmatched_track_ids(unmatched_tracks)
        print("---- Unmatched Tracks: {} ----".format(x))
        print("================================================================================")
        print('\n\n')
        # --- Homology Logic Here ---
        homology_map = self._court_homography_map["stanford_brown"]
        homology_distance_dict = dict()

        for track, prev_frame_bbox_tpl in prev_frame_bbox_info.items():
            # if len(prev_frame_bbox) > 0:
            prev_frame_no, prev_frame_bbox = prev_frame_bbox_tpl
            point_1 = _get_homology_co_ord(curr_bbox_info, homology_map)
            point_2 = _get_homology_co_ord(prev_frame_bbox, homology_map)
            distance = _get_euclidian_distance(point_1, point_2)
            print(track, distance)
            # print("Multiplying Factor is: {}".format(frame_no - prev_frame_no))
            if distance < 60 * (frame_no - prev_frame_no):  # CHANGE THE THRESHOLD
                homology_distance_dict[track] = distance

        print("Holomogy Distance Dict")
        print(homology_distance_dict)
        sorted_homology_track_list = [(k, homology_distance_dict[k])
                                      for k in sorted(homology_distance_dict,
                                                      key=homology_distance_dict.get, reverse=False)]
        print(sorted_homology_track_list)
        if len(sorted_homology_track_list) != 0:
            first_track_id_in_sorted_list = sorted_homology_track_list[0][0]
            to_be_assigned_track = [t for t in self.tracks if t.track_id == first_track_id_in_sorted_list][0]
            index = self.tracks.index(to_be_assigned_track)

            if 186 < frame_no < 196:
                print("================================================================================")
                print("-- DEBUG IN HOMOLOGY-- ")
                print("---- Frame in Consideration: {} ----".format(frame_no))
                print("---- Unmatched BBox: {} ----".format(curr_bbox_info))
                print("---- Assigned to Track {} ----".format(to_be_assigned_track.track_id))
                print("================================================================================")
                print('\n\n')
            unmatched_tracks.remove(index)

            # print("Track {} Index {}".format(to_be_assigned_track.track_id, index))
            print("Performed Homology")
            print("---- Track {} assigned the detection ----".format(self.tracks[index].track_id))
            self.tracks[index].update(self.kf, detection)
            # Do we need to update matches?
            return unmatched_tracks, matches
        else:
            print("================================================================================")
            print("-- DEBUG NOT IN HOMOLOGY -- ")
            print("---- Frame in Consideration: {} ----".format(frame_no))
            print("---- Unmatched BBox: {} ----".format(curr_bbox_info))
            print("Did Not Perform Homology")
            print("================================================================================")

            print('\n\n')

            return unmatched_tracks, matches

    def _perform_homology_and_centroid_based_allocation(self, curr_bbox_info, detection, unmatched_tracks, matches,
                                                        frame_no):
        unmatched_tracks_h, matches_h = self.perform_homology_based_allocation(
            curr_bbox_info, detection, unmatched_tracks, matches, frame_no)
        print("Homology Unmatched Tracks", unmatched_tracks_h)
        print("Passed Unmatched Tracks", unmatched_tracks)
        if (len(unmatched_tracks_h) == len(unmatched_tracks)) and len(unmatched_tracks_h) != 0:
            unmatched_tracks_c, matches_c = self.perform_centroid_based_allocation(
                curr_bbox_info, detection, unmatched_tracks_h, matches_h, frame_no)
            return unmatched_tracks_c, matches_c
        else:
            return unmatched_tracks_h, matches_h

    def _assign_detection_to_track(self, detection, unmatched_tracks):
        # Only add detection to unmatched tracks.
        matches = None

        print(detection)
        frame_no, curr_bbox_info = detection[1]
        print(frame_no, curr_bbox_info)

        # unmatched_tracks, matches = self._perform_centroid_based_allocation(curr_bbox_info, detection, unmatched_tracks, matches, frame_no)
        unmatched_tracks, matches = self._perform_homology_based_allocation(curr_bbox_info, detection, unmatched_tracks,
                                                                            matches, frame_no)
        # unmatched_tracks, matches = self._perform_homology_and_centroid_based_allocation(curr_bbox_info, detection, unmatched_tracks, matches, frame_no)

        return unmatched_tracks, matches

    def _handle_unmatched_detection_and_tracks(self, detections_copy, matches, unmatched_tracks, unmatched_detections):
        """
        Every unmatched detection needs to map to one unmatched track till there are no tracks that are unmatched.

        unmatched_tracks > 0 and unmatched_detections > 0
            assign an unmatched detection to an unmatched track.
            Use pixel distance from the detection to the predicted kf box or past frame
            Be greedy (i.e. choose the closet track for that box)
            after every match, unmatched tracks and unmatched detections will decrease by one.

        unmatched_tracks = 0 and unmatched_detections > 0 => initalize new tracks
        unmatched_tracks >0 and unmatched_detections = 0 => do nothing

        """

        # Drive either unmatched_tracks or unmatched_detections to zero.
        while len(unmatched_tracks) > 0 and len(unmatched_detections) > 0:
            detection_idx = unmatched_detections[0]
            unmatched_tracks, new_matches = \
                self._assign_detection_to_track(detections_copy[detection_idx], unmatched_tracks)
            unmatched_detections.remove(detection_idx)
            # update matches with new_matches.
            # e.g. matches = matches + new_matches

        if len(self.tracks) + len(self.deleted_tracks) > 12:
            print("We have more than 12 tracks!!!!!.")
            raise (ValueError(" error"))

        if len(unmatched_detections) == 0:
            # Do Nothing.
            return matches, unmatched_tracks, []

        if len(unmatched_tracks) == 0 and len(unmatched_detections) > 0:
            # Initialize new Tracks.
            for detection_idx in unmatched_detections:
                # print('--- detection idx ---', detection_idx)
                self._initiate_track(detections_copy[detection_idx])
                # print("# Tracks after Initialization: {}".format(len(self.tracks)))
            return matches, unmatched_tracks, []

        print("SHOULD NOT REACH HERE.")
        sys.exit(-1)

    def _initiate_track(self, detection):
        bbox_info = detection[1]

        detection = detection[0]
        print("-- Initiating Track With Track Index {}".format(self._next_id))
        print()
        mean, covariance = self.kf.initiate(detection.to_xyah())
        self.tracks.append(Track(
            mean, covariance, self._next_id, self.n_init, self.max_age,
            detection.feature, bbox_info))
        self._next_id += 1
