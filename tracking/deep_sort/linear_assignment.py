# vim: expandtab:ts=4:sw=4
from __future__ import absolute_import
import numpy as np
from sklearn.utils.linear_assignment_ import linear_assignment
from . import kalman_filter

INFTY_COST = 1e+5
_court_homography_map = dict()

_max_homology_distance = 60

_court_homography_map["stanford_orig"] = \
    np.array([[2.54189554e+00, 2.05548343e+00, -1.71490081e+03],
              [4.59969941e-02, 1.45810471e+01, -4.16148605e+03],
              [3.71823046e-05, 2.65221085e-03, 1.00000000e+00]])

_court_homography_map["stanford_480"] = \
    np.array([[3.58116231e+00, 2.49261517e+00, -1.39335611e+03],
              [-1.85560132e-01, 2.08755149e+01, -4.27576935e+03],
              [1.96505291e-06, 3.80235925e-03, 1.00000000e+00]])

_court_homography_map["stanford_blue"] = \
    np.array([[1.38019562e+00, 4.16755338e-01, -4.87353692e+02],
              [3.60155502e-02, 5.44555119e+00, -8.60526743e+02],
              [1.07991316e-05, 9.72631616e-04, 1.00000000e+00]])

_court_homography_map["stanford_brown"] = \
    np.array([[1.40750079e+00, 4.28041386e-01, -5.01996866e+02],
              [3.69169984e-02, 5.53754976e+00, -8.97378396e+02],
              [1.67593482e-05, 9.94257909e-04, 1.00000000e+00]])


def get_homology_cost_info_for_det(last_active_frame_track_bbox_info, bbox_info, frame_no):
    def _get_homology_co_ord(bbox, homology_matrix):
        width = int(bbox[2])
        height = int(bbox[3])

        left_x = int(bbox[0])
        top_y = int(bbox[1])
        right_x = left_x + width
        bottom_y = top_y + height

        x = (left_x + right_x) / 2.0
        y = bottom_y

        p = np.array((x, y, 1)).reshape((3, 1))
        temp_p = homology_matrix.dot(p)
        sum_z = np.sum(temp_p, 1)

        bottom_homology_x = int(round(sum_z[0] / sum_z[2]))
        bottom_homology_y = int(round(sum_z[1] / sum_z[2]))

        return bottom_homology_x, bottom_homology_y

    def _get_euclidian_distance(point_1, point_2):
        x1, y1 = point_1
        x2, y2 = point_2
        return np.sqrt(((x2 - x1) ** 2) + ((y2 - y1) ** 2))

    # (xtl, ytl) => (709, 217)
    # (xbr, xbr) => (794, 462)

    # --- Homology Logic Here ---
    homology_map = _court_homography_map["stanford_brown"]
    homology_distance_list = list()

    prev_frame_no, prev_frame_bbox = last_active_frame_track_bbox_info
    point_1 = _get_homology_co_ord(prev_frame_bbox, homology_map)

    for info in bbox_info:
        frame_no, bbox = info
        point_2 = _get_homology_co_ord(bbox, homology_map)
        distance = _get_euclidian_distance(point_1, point_2)

        homology_distance_list.append(distance)
    return homology_distance_list


def min_cost_matching(
        distance_metric, max_distance, tracks, detections, track_indices=None,
        detection_indices=None, bbox_info=None):
    """Solve linear assignment problem.

    Parameters
    ----------
    distance_metric : Callable[List[Track], List[Detection], List[int], List[int]) -> ndarray
        The distance metric is given a list of tracks and detections as well as
        a list of N track indices and M detection indices. The metric should
        return the NxM dimensional cost matrix, where element (i, j) is the
        association cost between the i-th track in the given track indices and
        the j-th detection in the given detection_indices.
    max_distance : float
        Gating threshold. Associations with cost larger than this value are
        disregarded.
    tracks : List[track.Track]
        A list of predicted tracks at the current time step.
    detections : List[detection.Detection]
        A list of detections at the current time step.
    track_indices : List[int]
        List of track indices that maps rows in `cost_matrix` to tracks in
        `tracks` (see description above).
    detection_indices : List[int]
        List of detection indices that maps columns in `cost_matrix` to
        detections in `detections` (see description above).
    # Added by rtailor
    bbox_info : List[(int, np.ndArray)]
        List of tuples (frame_no, bbox_array)

    Returns
    -------
    (List[(int, int)], List[int], List[int])
        Returns a tuple with the following three entries:
        * A list of matched track and detection indices.
        * A list of unmatched track indices.
        * A list of unmatched detection indices.

    """

    # I will need dets bbox info and frame number in this function

    def homographic_distance(tracks, bbox_info, curr_frame_no):
        # Here I need detection tuple with bbox information
        # dets should be bbox array for current iteration

        cost_matrix = np.zeros((len(tracks), len(bbox_info)))

        for i, track in enumerate(tracks):
            last_active_frame_track_bbox_info = track.detections[-1]
            last_active_frame = last_active_frame_track_bbox_info[0]
            cost_matrix[i, :] = get_homology_cost_info_for_det(last_active_frame_track_bbox_info, bbox_info,
                                                               curr_frame_no)
            cost_matrix[cost_matrix > (
                    _max_homology_distance * (curr_frame_no - last_active_frame))] = False
            cost_matrix[cost_matrix != False] = True
        return cost_matrix

    def spordia_metric(tracks, track_indices, detection_indices, bbox_info):

        # Here I need frame information somehow
        print('-- spordia metric debug --')
        print('# Tracks {}'.format(len(tracks)))
        print('# bbox_info {}'.format(len(bbox_info)))
        targets = np.array([tracks[i] for i in track_indices])
        print('# targets {}'.format(len(targets)))
        # print("bbox info {}".format(bbox_info))
        bbox_info_subset = np.array([bbox_info[i] for i in detection_indices])
        frame_no = bbox_info[0][0]
        cost_matrix = homographic_distance(targets, bbox_info_subset, frame_no)

        return cost_matrix

    if track_indices is None:
        track_indices = np.arange(len(tracks))
    if detection_indices is None:
        detection_indices = np.arange(len(detections))

    if len(detection_indices) == 0 or len(track_indices) == 0:
        return [], track_indices, detection_indices  # Nothing to match.

    cost_matrix = distance_metric(
        tracks, detections, track_indices, detection_indices)

    # print('-- cost matrix shape --')
    # print(cost_matrix.shape)

    # Added by sunilkul.
    # Add conditions to prevent tele-portation for the boxes.
    # valid_matches = [] N x M array of Binary.
    # cost_matrix[valid_matches != True] = max_distance + 1e-5

    spordia_cost_matrix = spordia_metric(tracks, track_indices, detection_indices, bbox_info)

    cost_matrix[spordia_cost_matrix == False] = max_distance + 1e-5

    indices = linear_assignment(cost_matrix)

    print("Indices", indices)

    matches, unmatched_tracks, unmatched_detections = [], [], []
    for col, detection_idx in enumerate(detection_indices):
        if col not in indices[:, 1]:
            unmatched_detections.append(detection_idx)
    for row, track_idx in enumerate(track_indices):
        if row not in indices[:, 0]:
            unmatched_tracks.append(track_idx)
    for row, col in indices:
        track_idx = track_indices[row]
        detection_idx = detection_indices[col]
        if cost_matrix[row, col] > max_distance:
            unmatched_tracks.append(track_idx)
            unmatched_detections.append(detection_idx)
        else:
            print("=====> Track {} With T_Idx {} Matched with Detection {} having D_Idx {}<=====".format(tracks[track_idx].track_id, track_idx, bbox_info[detection_idx], detection_idx))
            matches.append((track_idx, detection_idx))
    print("====> After Indices For Loop <====")
    for t_idx, d_idx in matches:
        print("=====> T_Idx {} Matched with D_Idx {}<=====".format(t_idx, d_idx))
    return matches, unmatched_tracks, unmatched_detections


def matching_cascade(
        distance_metric, max_distance, cascade_depth, tracks, detections,
        track_indices=None, detection_indices=None, bbox_info=None):
    """Run matching cascade.

    Parameters
    ----------
    distance_metric : Callable[List[Track], List[Detection], List[int], List[int]) -> ndarray
        The distance metric is given a list of tracks and detections as well as
        a list of N track indices and M detection indices. The metric should
        return the NxM dimensional cost matrix, where element (i, j) is the
        association cost between the i-th track in the given track indices and
        the j-th detection in the given detection indices.
    max_distance : float
        Gating threshold. Associations with cost larger than this value are
        disregarded.
    cascade_depth: int
        The cascade depth, should be se to the maximum track age.
    tracks : List[track.Track]
        A list of predicted tracks at the current time step.
    detections : List[detection.Detection]
        A list of detections at the current time step.
    track_indices : Optional[List[int]]
        List of track indices that maps rows in `cost_matrix` to tracks in
        `tracks` (see description above). Defaults to all tracks.
    detection_indices : Optional[List[int]]
        List of detection indices that maps columns in `cost_matrix` to
        detections in `detections` (see description above). Defaults to all
        detections.

    Returns
    -------
    (List[(int, int)], List[int], List[int])
        Returns a tuple with the following three entries:
        * A list of matched track and detection indices.
        * A list of unmatched track indices.
        * A list of unmatched detection indices.

    """
    if track_indices is None:
        track_indices = list(range(len(tracks)))
    if detection_indices is None:
        detection_indices = list(range(len(detections)))

    unmatched_detections = detection_indices
    matches = []
    for level in range(cascade_depth):
        print("===> Level {} <===".format(level))
        if len(unmatched_detections) == 0:  # No detections left
            break

        track_indices_l = [
            k for k in track_indices
            if tracks[k].time_since_update == 1 + level
        ]
        if len(track_indices_l) == 0:  # Nothing to match at this level
            continue
        print("===> Calling min_cost_matching function for level {} <===".format(level))
        matches_l, _, unmatched_detections = \
            min_cost_matching(
                distance_metric, max_distance, tracks, detections,
                track_indices_l, unmatched_detections, bbox_info)
        matches += matches_l
    unmatched_tracks = list(set(track_indices) - set(k for k, _ in matches))
    return matches, unmatched_tracks, unmatched_detections


def gate_cost_matrix(
        kf, cost_matrix, tracks, detections, track_indices, detection_indices,
        gated_cost=INFTY_COST, only_position=False):
    """Invalidate infeasible entries in cost matrix based on the state
    distributions obtained by Kalman filtering.

    Parameters
    ----------
    kf : The Kalman filter.
    cost_matrix : ndarray
        The NxM dimensional cost matrix, where N is the number of track indices
        and M is the number of detection indices, such that entry (i, j) is the
        association cost between `tracks[track_indices[i]]` and
        `detections[detection_indices[j]]`.
    tracks : List[track.Track]
        A list of predicted tracks at the current time step.
    detections : List[detection.Detection]
        A list of detections at the current time step.
    track_indices : List[int]
        List of track indices that maps rows in `cost_matrix` to tracks in
        `tracks` (see description above).
    detection_indices : List[int]
        List of detection indices that maps columns in `cost_matrix` to
        detections in `detections` (see description above).
    gated_cost : Optional[float]
        Entries in the cost matrix corresponding to infeasible associations are
        set this value. Defaults to a very large value.
    only_position : Optional[bool]
        If True, only the x, y position of the state distribution is considered
        during gating. Defaults to False.

    Returns
    -------
    ndarray
        Returns the modified cost matrix.

    """
    gating_dim = 2 if only_position else 4
    gating_threshold = kalman_filter.chi2inv95[gating_dim]
    measurements = np.asarray(
        [detections[i].to_xyah() for i in detection_indices])
    for row, track_idx in enumerate(track_indices):
        track = tracks[track_idx]
        gating_distance = kf.gating_distance(
            track.mean, track.covariance, measurements, only_position)
        cost_matrix[row, gating_distance > gating_threshold] = gated_cost
    return cost_matrix
