import os
import xml.etree.ElementTree as ET

def get_convert_xml_2(tree,i):
    root = tree.getroot()
    omit = ['ball', 'refree']
    for child in root.findall(".//object"):
        if child.find('name').text == 'pole':
            root.remove(child)
        if child.find('name').text.strip() not in omit:
            print('--- name ---', child.find('name').text, '-- changed --')
            child.find('name').text = 'player'
        root.find('filename').text = i.split(".")[0] + ".jpg"
    indent(root)
    return tree

def indent(elem, level=0):
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i


xml_path = "/home/usergpu/train_xml/"
convert_path = "./data/VOCdevkit/VOC2007/AnnotationsNew/"

list_txt = [i for i in os.listdir(xml_path) if len(i.split('.')) == 2 and i.split('.')[1] == 'xml']

for i in list_txt:
    tree = ET.parse(xml_path + i)
    converted_tree = get_convert_xml_2(tree,i)
    converted_tree.write(convert_path + i)
    print(i)
